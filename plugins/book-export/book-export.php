<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Plugin;
use Grav\Common\Page\Collection;
use Grav\Common\Uri;
use Grav\Common\Taxonomy;
use \Grav\Common\Page\Page;
use \Grav\Common\Page\Header;
use \Grav\Common\Themes;

use Goutte\Client;
use domDocument;
/**
 * Class BookExportPlugin
 * @package Grav\Plugin
 */
class BookExportPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'onPluginsInitialized' => [
                // Uncomment following line when plugin requires Grav < 1.7
                // ['autoload', 100000],
                ['onPluginsInitialized', 0]
            ]
        ];
    }

    /**
     * Composer autoload
     *
     * @return ClassLoader
     */
    public function autoload(): ClassLoader
    {
        return require __DIR__ . '/vendor/autoload.php';
    }

    

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized(): void
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            $this->enable([
                'onAssetsInitialized' => ['onAssetsInitialized', 0]
            ]);
        }

        // Enable the main events we are interested in
        $this->enable([
            'onPageInitialized' => ['onPageInitialized', 0]
            // Put your main events here
        ]);
    }
    // test ajout js to admin
    public function onAssetsInitialized() {

            // add JS
            $this->grav['assets']->addJs('theme://js/admin_tinymcepatch.js');

    }
    public function onPageInitialized(): void{
        //display button
        $uri = $this->grav['uri'];
        $page = $this->grav['page'];
        // print_r($uri->base());
        if($uri->params() == "/web:true"){
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                //create folder to store static website for now it is at the root
                $dir="book-export/".$page->slug();
                
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }
                //init crawler
                $url = $uri->base().$uri->url().'/web:true';
                $web = new \Spekulatius\PHPScraper\PHPScraper;
                $web->go($url);
                
                //dowload images
                $images_array = $web->imagesWithDetails;
                foreach($images_array as $image){
                    $img=file_get_contents($image["url"]);
                    $imgname=explode("/",$image["url"]);
                    $imgname=$imgname[sizeof($imgname)-1];
                    
                    file_put_contents($dir."/".$imgname, $img);
                }

                //dowload font from page content and store name for regex 
                
                $header = $page->header();
                $fontNames = [];
                foreach($header->fontsList as $font){
                    
                    $file = $uri->base().$uri->url().'/'.$font['fontFile'];
                    //$font_file = file_get_contents($file);
                    // var_dump($file);
                    array_push($fontNames, $font['fontFile']);
                    //move_uploaded_file($font_file, $dir."/".$font['fontFile']);
                    copy($file,$dir."/".$font['fontFile'] );
                    //file_put_contents( $file , );

                }
              

                //var_dump($web->textContent());
                
                // download page content with cUrl
                
                $ch = curl_init($url);
                $fp = fopen($dir.'/'."index.html", "w");

                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);

                curl_exec($ch);
                if(curl_error($ch)) {
                    fwrite($fp, curl_error($ch));
                }
                curl_close($ch);
                fclose($fp);

                // DOM rewritting urls
                $html = file_get_contents($dir.'/'."index.html");
                
                $doc = new DOMDocument;
                $doc->loadHTML($html);
                $doc->formatOutput = true;
                //rename images
                $tags = $doc->getElementsByTagName('img');
                foreach($tags as $tag)
                {
                    $imgname=explode("/",$tag->getAttribute("src"));
                    $imgname=$imgname[sizeof($imgname)-1];
                    $tag->setAttribute('src', $imgname);
                    $doc->saveHTML($tag);
                }
                //rename script
                $tags = $doc->getElementsByTagName('script');
                //patch for localhost
                $base = 'http://'.($_SERVER['SERVER_NAME']==='localhost')?'http://localhost':$_SERV‌​ER['SERVER_NAME']; 
                foreach($tags as $tag)
                {
                    $src = $tag->getAttribute("src");
                    $scriptname=explode("/",$src);
                    $scriptname=$scriptname[sizeof($scriptname)-1];
                    //download them ! 
                    $file=file_get_contents($base.$tag->getAttribute("src"));

                    file_put_contents($dir."/".$scriptname, $file );                 
                    $tag->setAttribute('src', $scriptname);
                    $doc->saveHTML($tag);
                }
                
                //find css font and change url
                $tags = $doc->getElementsByTagName('style');
                if(count($tags) > 0)
                {
                    $tag = $tags->item(0); // the first style is about fonts
                    $fontrules = explode("@font-face", trim(preg_replace('/\s\s+/', ' ', $tag->nodeValue)));
                    $fontrules = array_values(array_filter($fontrules));
                    $newrules  = [];
                    foreach($fontrules as $key => $font){
                        $search = '/(url\(\")(.*?)(\"\))/';

                        $replace = '$1'.$fontNames[$key].'$3';
                        array_push($newrules, preg_replace($search,$replace,$font));
                    }
                    $string = "@font-face".implode("@font-face",$newrules);
                    $tag->nodeValue = $string;
                    $doc->saveHTML($tag);
                }
                //remove node form 
                $tags = $doc->getElementsByTagName('form');
                if(count($tags) > 0)
                {
                    $tag = $tags->item(0); // the first style is about fonts
                    $tag->parentNode->removeChild($tag);

                }
                

                $doc->saveHTMLFile($dir.'/'."index.html"); 
                
                // Files which need to be added into zip
                $files = glob($dir . "/*") ;
                
                // Directory of files
                //$filesPath = $dir;
                // Name of creating zip file
                $zipName = $page->slug().'_web-static'.'.zip';

                echo createZipAndDownload($files, $zipName);
                
            }
              
        }

    }
}

function createZipAndDownload($files, $zipFileName)
{
    // Create instance of ZipArchive. and open the zip folder.
    $zip = new \ZipArchive();
    if ($zip->open($zipFileName, \ZipArchive::CREATE) !== TRUE) {
        exit("cannot open <$zipFileName>\n");
    }

    // Adding every attachments files into the ZIP.
    foreach ($files as $file) {
        $zip->addFile($file, $file);
    }
    $zip->close();

    // Download the created zip file
    header("Content-type: application/zip");
    header("Content-Disposition: attachment; filename = $zipFileName");
    header("Pragma: no-cache");
    header("Expires: 0");
    readfile("$zipFileName");
    exit;
}

