# ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Diablo_legas.svg/234px-Diablo_legas.svg.png) Bookolab

Colaborative book generator base on [Grav](http://getgrav.org), [Togetherjs](https://togetherjs.com/) and [Pagedjs](https://pagedjs.org).

This the account folder of a normal grav install.   
[First install grav](https://learn.getgrav.org/17/basics/installation)
Then replace the user folder with this folder, but keep your own ```accounts/``` and ```pages/```.

## Plugins required for grav 

- Admin 
- [Git-sync](https://getgrav.org/blog/git-sync-plugin)
- NextGen editor (facultatif) -> tinymce now
- Admin Addon Media Metadata

## How to create a stand alone togetherJS hub and link it to grav

The necessary files are stored in `data/_hub-config`.    
Then edit link in the plugin ```togetherHub``` in the admin panel.

## Admin

***

1. specific fields : 
```
header.test: 
              type: starterButton
              label: start Together Js
 ```

## Todo

- [x] create method note and ref

- [x] ajustement drapeau with ragajust (fr)

- [ ] orphan and widow -> ?

- [x] Gestion des césures (fr)

- [ ] interface pour designer grapĥic (preview mode et affichage grid) <

- [ ] Generate static website from book page <

- [ ] Specific git for each book  

- [ ] preview/save/publish workflow plugin

- [ ] images captions


